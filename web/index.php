<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$fsRoot = dirname(__DIR__);

$app = new Silex\Application();
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => 'php://stdout',
));

$router = new \Intu\Router($app['monolog']);
$bbToJenkins = new \Intu\Handlers\BitBucketToJenkinsHandler($fsRoot . '/config/bitbucket-jenkins.json');
$bbToJenkins->setLogger($app['monolog']);
$router->addHandler($bbToJenkins);

$app->post('/{key}', function (Request $request, $key) use($router) {
    $router->dispatch($request, $key);
    return new Response('ok', 201);
})->value('key', 'unknownKey');

$app->run();