<?php
/**
 * BitBucketToJenkinsHandler.php
 * @author ciaran
 * @date 10/07/15 11:24
 *
 */

namespace Intu\Handlers;

use Intu\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class BitBucketToJenkinsHandler implements RequestHandlerInterface, LoggerAwareInterface
{
    protected $config;

    /** @var  LoggerInterface */
    protected $logger;

    function __construct($configFileName = null)
    {
        if($configFileName && file_exists($configFileName)) {
            $rawJson = file_get_contents($configFileName);

            $this->config = json_decode($rawJson);

        }
    }

    public function canHandle($key, Request $request)
    {
        if($key !== "bitbucket") {
            return false;
        }

        if($request->getRealMethod() !== "POST") {
            return false;
        }

        $payload = $request->request->get('payload', null);
        if(!$payload) {
            return false;
        }

        return true;
    }

    public function handle(Request $request)
    {
        $rawPayload = $request->request->get('payload');

        $payload = json_decode($rawPayload, true);
        $absoluteUrl = $payload['repository']['absolute_url'];

        if(!$absoluteUrl) {
            return;
        }

        foreach($this->config as $mappingConfig) {
            $matches = [];
            preg_match("@".$mappingConfig->bitbucket_regexp."@", $absoluteUrl, $matches);
            if($matches && array_key_exists('reponame', $matches)) {
                $jenkinsUrl = str_replace('%reponame%', $matches['reponame'], $mappingConfig->jenkins_url);
                if($this->logger) {
                    $this->logger->info(
                        sprintf("Matched %s",
                            $jenkinsUrl
                        )
                    );
                }
            }
        }
        //$payload->repository->absolute_url === '/intudigital/intu-wifi';
    }

    /**
     * Sets a logger instance on the object
     *
     * @param LoggerInterface $logger
     * @return null
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


}