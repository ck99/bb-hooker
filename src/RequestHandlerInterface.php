<?php
/**
 * RequestHandlerInterface.php
 * @author ciaran
 * @date 10/07/15 11:04
 *
 */

namespace Intu;


use Symfony\Component\HttpFoundation\Request;

interface RequestHandlerInterface
{
    public function canHandle($key, Request $request);
    public function handle(Request $request);
}