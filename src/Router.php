<?php

namespace Intu;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class Router
{
    /** @var RequestHandlerInterface[] */
    private $handlers;

    /** @var LoggerInterface  */
    private $logger;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->handlers = [];
        $this->logger = $logger;
    }

    public function addHandler(RequestHandlerInterface $handler)
    {
        $this->handlers[] = $handler;
    }

    public function dispatch(Request $request, $key = null)
    {
        $counter = 0;
        foreach($this->handlers as $handler) {
            if($handler->canHandle($key, $request)) {
                $handler->handle($request);
                $counter++;
                $this->logInfo(sprintf(
                    "%s executed.",
                    get_class($handler)
                ));
            }
        }

        $this->logInfo(sprintf(
            "Request Key: %s : %d handlers fired.",
            $key,
            $counter
        ));
    }

    private function logInfo($message)
    {
        if($this->logger) {
            $this->logger->info($message);
        }
    }

    private function logDebug($message)
    {
        if($this->logger) {
            $this->logger->debug($message);
        }
    }
}